module.exports = function (grunt) {

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          'public/assets/stylesheets/style.css': 'public/assets/less/style.less'
        }
      }
    },
    watch: {
      styles: {
        files: ['public/assets/less/*.less'],
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('compile', ['less', 'watch']);


};
