var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Section = require('../db/sectionSchema');

/* GET users listing. */
router.get('/sections', function(req, res, next) {

  Section.find({}, function(err, sections) {
    if (err) {
      console.log(err);
      res.send(err);
    }
    res.json(sections); // return all todos in JSON format
  });

});

router.get('/categories', function(req, res, next) {
    var categories = [{label :'Accessories', id:2042}, {label : 'Jackets', id:1510}, {label : 'T-shirts', id:1921}];
    res.json(categories);

});

module.exports = router;
