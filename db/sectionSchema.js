// import the mongoose package
var mongoose = require('mongoose'); // import the model

// define the schema
var sectionSchema = new mongoose.Schema({
    label: String,
    href: String
}, {collection: 'sections'});

// compile schema into a model
var Section = mongoose.model('Section', sectionSchema);

// expose the schema
module.exports = Section;