// import the mongoose package
var mongoose = require('mongoose');
var config = {
    url: 'mongodb://localhost/easy-purchase'
};
mongoose.connect(config.url, function (error) {
    if (error) {
        console.log(error);
    }
});

// connection events: 
// we can use the mongoose api to hook into events e.g. when connecting / disconnecting to MongoDB
mongoose.connection.on('connected', function() {
    console.log('Connected to url: ' + config.url);
});
mongoose.connection.on('error', function(err) {
    console.log('Connection error: ' + err);
});

// import the todoSchema
require('./sectionSchema.js');