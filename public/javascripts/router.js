define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'src/controllers/appCtrl'
],

function ($, _, Backbone, Marionette, Store) {

  'use strict';

  return  Marionette.AppRouter.extend({

    appRoutes : {
      '' : 'store#renderHomePage',
      'products?*queryString' : 'store#renderProductsPage'
    },


    initialize : function () {
      this.initControllers();
    },

    initControllers : function () {
      this.store = new Store();
    }


  });

});

