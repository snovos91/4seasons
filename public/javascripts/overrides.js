define([
  'jquery',
  'underscore',
  'backbone',
  'marionette'
],

function ($, _, Backbone, Marionette) {

  'use strict';

  return function () {
    Marionette.TemplateCache.prototype.fetchTemplate = function (name) {
      if (_.isFunction(name)) {
        return name;
      }
      var template = window.JST && window.JST[name];
      if (!template) {
        var url = 'javascripts/src/templates/' + name + '.html';
        $.ajax({url: url, async: false}).then(function (contents) {
          template = _.template(contents);
        });
      }
      return template;
    };

  Marionette.AppRouter.prototype.processAppRoutes = function (controller, appRoutes) {
    var routeNames = _.keys(appRoutes).reverse(); // Backbone requires reverted order of routes
    var self = this;
    _.each(routeNames, function (route) {
      var methodName = appRoutes[route];
      var params = methodName.split('#');
      var ctrl = controller || self;
      var method;

      if (params.length > 1) {
        var ctrlName = params[0];
        ctrl = this[ctrlName] || this.options[ctrlName];
        methodName = params[1];
        if (!ctrl || !methodName) {
          throw new Error('Please specify correct route');
        }
      }
      method = ctrl[methodName];

      if (!method) {
        throw new Error('Method "' + methodName + '" was not found in the controller');
      }

      this.route(route, function () {
        this.trigger('before:route:change');
        method.apply(ctrl, arguments);
      });

    }, this);
  };


  Marionette.TemplateCache.prototype.loadTemplate = function (templateId) {
    return this.fetchTemplate(templateId);
  };

  Marionette.TemplateCache.prototype.compileTemplate = function (template) {
    return template;
  };
}

});