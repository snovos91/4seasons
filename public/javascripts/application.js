define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'overrides',
  'globals'
],

function ($, _, Backbone, Marionette, overrides, globals) {

  'use strict';

  var Application = Marionette.Application.extend({

    utils : globals.utils,
    constants : globals.constants

  });

  overrides();

  return Application;

});

