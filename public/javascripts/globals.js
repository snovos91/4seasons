define([],

  function () {

    'use strict';

    var globals = {};

    globals.variables = {};

    globals.constants = {
      url : {
        products : 'products/genderName={0}'
      }
    };

    globals.utils = {
      toFragment : function(route, queryParameters) {
        if (queryParameters) {
          if (!_.isString(queryParameters)) {
            queryParameters = this.toQueryString(queryParameters);
          }
          if(queryParameters) {
            route += '?' + queryParameters;
          }
        }
        return route;
      },


    /**
     * Serialize the val hash to query parameters and return it.  Use the namePrefix to prefix all param names (for recursion)
     */
    toQueryString : function (val, namePrefix) {
      /*jshint eqnull:true */
      var splitChar = Backbone.Router.arrayValueSplit;
      function encodeSplit(val) { return String(val).replace(splitChar, encodeURIComponent(splitChar)); }

      if (!val) {
        return '';
      }

      namePrefix = namePrefix || '';
      var rtn = [];
      _.each(val, function(_val, name) {
        name = namePrefix + name;

        if (_.isString(_val) || _.isNumber(_val) || _.isBoolean(_val) || _.isDate(_val)) {
          // primitive type
          if (_val != null) {
            rtn.push(name + '=' + encodeSplit(encodeURIComponent(_val)));
          }
        } else if (_.isArray(_val)) {
          // arrays use Backbone.Router.arrayValueSplit separator
          var str = '';
          for (var i = 0; i < _val.length; i++) {
            var param = _val[i];
            if (param != null) {
              str += splitChar + encodeSplit(encodeURIComponent(param));
            }
          }
          if (str) {
            rtn.push(name + '=' + str);
          }
        } else {
          // dig into hash
          var result = this.toQueryString(_val, name + '.');
          if (result) {
            rtn.push(result);
          }
        }
      });

      return rtn.join('&');
    },
      parseQueryString : function (queryString) {
          var params = {};
          if(queryString){
            _.each(
                _.map(decodeURI(queryString).split(/&/g),function(el,i){
                  var aux = el.split('='), o = {};
                  if(aux.length >= 1){
                    var val = undefined;
                    if(aux.length == 2)
                      val = aux[1];
                    o[aux[0]] = val;
                  }
                  return o;
                }),
                function(o){
                  _.extend(params,o);
                }
            );
          }
          return params;
    },
      substitute : function (string) {
        if (!string) {
          return '';
        }
        var values = _.rest(arguments);
        var result = string;
        _.each(values, function (value, index) {
          var reg = new RegExp('\\{' + index + '\\}');
          result = result.replace(reg, value);
        });
        return result;
      }
    };

    return globals;

  });
