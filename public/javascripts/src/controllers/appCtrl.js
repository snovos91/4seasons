define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'app',
    'src/views/layout',
    'src/models/queryModel',
    'src/models/categoryModel',
    'src/views/header/headerLayoutView',
    'src/views/product/productsLayout'
],

function ($, _, Backbone, Marionette, app, Layout, QueryModel, CategoryModel, HeaderView, ProductsLayout) {

    'use strict';

    return Marionette.Controller.extend({

        initialize : function () {
            this.initLayout();
            this.initStaticComponents();
            this.addEventListeners();
        },


        addEventListeners : function () {
            this.listenTo(this.queryModel, 'change:genderName', this.onGenderNameChange);
        },


        initLayout : function () {
            this.layout = new Layout();
            this.layout.render();
        },


        onGenderNameChange : function () {
            var route = app.utils.toFragment('products', this.queryModel.toJSON());
            app.router.navigate(route, {trigger:true});
        },



        initStaticComponents : function () {
            this.queryModel = new QueryModel();
            var CategoriesCollection = Backbone.Collection.extend({
                url : '/api/categories',
                model : CategoryModel
            });
            this.categoriesCollection = new CategoriesCollection();
            this.headerView = new HeaderView({queryModel:this.queryModel});
            this.headerView.render();
        },

        renderHomePage : function () {
            console.log('home');
        },


        renderProductsPage : function (options) {
            this.queryModel.setParsedQuery(options);
            this.productsLayout = new ProductsLayout({queryModel:this.queryModel});
            this.productsLayout.render();
        },


        showStaticComponents : function () {
            this.categoriesCollection.fetch().done(this.onCategoriesFetch);
            this.headerView.render();
        },

        onCategoriesFetch : function () {
            this.layout.menuRegion.show(this.sideBarView);
        }

    });

});
