define([
    'jquery',
    'underscore',
    'backbone',
    'marionette'
],

function ($, _, Backbone, Marionette) {

    'use strict';

    return Marionette.ItemView.extend({
        className : 'product-item',
        template : 'product/product-item'

    });

});
