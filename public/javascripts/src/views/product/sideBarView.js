define([
    'jquery',
    'underscore',
    'backbone',
    'marionette'
],

function ($, _, Backbone, Marionette) {

    'use strict';

    return Marionette.CollectionView.extend({
        tagName : 'ul',
        template : 'side-bar',
        getChildView : function (item) {
            return Marionette.ItemView.extend({
                tagName : 'li',
                template : 'menu-item',

                events : {
                    'click [data-category-link]' : 'onCategoryClickHandler'
                },

                onCategoryClickHandler : function (e) {
                    debugger;
                    e.preventDefault() ;
                    this.model.set('selected', true);
                }
            });

        }

    });

});
