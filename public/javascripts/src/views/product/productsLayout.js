define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'src/views/product/productsCollectionView',
    'src/collections/productsCollection',
        'src/collections/categoriesCollection',
    'src/views/product/sideBarView' ,
    'src/views/sales/salesView'
],

function ($, _, Backbone, Marionette, ProductsView, ProductsCollection, CategoriesCollection, SideBarView, SalesView) {

    'use strict';

    return Marionette.LayoutView.extend({
        el : '[data-content-region]',
        template : 'product/products-layout',
        regions : {
            menuRegion : '[data-side-region]',
            productsRegion : '[data-products-region]'
        },

        initialize : function () {
            this.initProductComponents();
            this.listenTo(this.sideBarView, 'itemview:show:products', this.showRelatedProducts);
        },


        showRelatedProducts : function (view) {
            console.log(view);
        },


        initProductComponents : function () {
            this.productsCollection = new ProductsCollection();
            this.categoriesCollection = new CategoriesCollection();
            this.sideBarView = new SideBarView({collection:this.categoriesCollection});
            this.salesView = new SalesView();
            this.productsView = new ProductsView({collection:this.productsCollection});
        },

        onRender : function () {
            this.categoriesCollection.setUrl(this.options.queryModel.toJSON());
            this.categoriesCollection.fetch().done(
                function() {
                    this.menuRegion.show(this.sideBarView);
                }
            );

        }
    });

});
