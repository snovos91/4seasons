define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'src/views/product/productItemView'
],

function ($, _, Backbone, Marionette, ProductItemView) {

    'use strict';

    return Marionette.CompositeView.extend({
        className : 'products-list',
        template : 'product/product-list',
        childViewContainer : '.product-list-wrapper',
        childView : ProductItemView

    });

});
