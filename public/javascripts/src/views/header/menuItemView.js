define([
    'jquery',
    'underscore',
    'backbone',
    'marionette'
],

function ($, _, Backbone, Marionette) {

    'use strict';

    return Marionette.ItemView.extend({

        tagName : 'li',
        template : 'header/menuItem',

        events : {
            'click [data-gender-option]' : 'genderOptionClickHandler'
        },

        genderOptionClickHandler : function (e) {
            e.preventDefault();
            this.options.queryModel.set('genderName', this.model.get('genderName'));
        }

    });

});
