define([
    'jquery',
    'underscore',
    'backbone',
    'marionette'
],

function ($, _, Backbone, Marionette) {

    'use strict';

    return Marionette.CompositeView.extend({

        className : 'search-panel container',
        template : 'header/search'

    });

});
