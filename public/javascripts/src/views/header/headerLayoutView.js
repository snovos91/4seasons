define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'src/views/header/headerMenuView',
    'src/views/header/headerSearchView',
    'src/collections/menuCollection',
    'src/models/cartModel'
],

function ($, _, Backbone, Marionette,  MenuView, SearchView, MenuCollection, CartModel) {

    'use strict';

    return Marionette.LayoutView.extend({
        el : '[data-header-region]',
        template : 'header/headerLayout',

        regions : {
            menuRegion : '[data-menu-region]',
            searchRegion : '[data-search-region]'
        },

        initialize : function (options) {
            _.bindAll(this, 'renderHeaderViews');
            this.menuCollection = new MenuCollection();
            this.cartModel = new CartModel();
            this.menuView = new MenuView({
                collection: this.menuCollection,
                model: this.cartModel,
                queryModel: options.queryModel
            });
            this.searchView = new SearchView();
        },

        onRender : function () {
            this.menuCollection.fetch().done(this.renderHeaderViews);
        },

        renderHeaderViews : function () {
            this.menuRegion.show(this.menuView);
            this.searchRegion.show(this.searchView);
        }
    });

});
