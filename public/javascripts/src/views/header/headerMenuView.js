define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'src/views/header/menuItemView'
],

function ($, _, Backbone, Marionette, MenuItemView) {

    'use strict';

    return Marionette.CompositeView.extend({
        className : 'top-bar',
        template : 'header/menu',
        childViewContainer : 'ul',
        childView : MenuItemView,
        childViewOptions : function () {
            return {queryModel: this.options.queryModel};
        }

    });

});
