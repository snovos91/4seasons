define([
    'jquery',
    'underscore',
    'backbone',
    'marionette'
],

function ($, _, Backbone, Marionette) {

    'use strict';

    return Backbone.Collection.extend({

        url : 'api/sections'

    });

});
