define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'app'
],

function ($, _, Backbone, Marionette, app) {

    'use strict';

    return Backbone.Collection.extend({

        url : 'products?{0}',

        setUrl : function (params) {
            this.url = app.utils.substitute(this.url, params);
        }

    });

});
