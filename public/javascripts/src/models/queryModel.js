define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'app'
],

function ($, _, Backbone, Marionette, app) {

    'use strict';

    return Backbone.Model.extend({
        defaults : {
            genderName : '',
            categoryId : ''
        },


        setParsedQuery : function (options) {
            var attrs = app.utils.parseQueryString(options);
            _.each(attrs, function (value, prop) {
                this.set(prop, value);
            }, this)
        }

    });

});
