require.config({
  'deps' : ['main'],
  'baseUrl' : 'javascripts',
  'paths' : {
    'libs' : '/assets/libs',
    'plugins' : '/assets/plugins',
    'underscore' : '/assets/libs/underscore',

    'jquery' : '/assets/libs/jquery',
    'backbone' : '/assets/libs/backbone',
    'marionette' : '/assets/libs/marionette',
    'backbone.stickit' : '/assets/plugins/backbone.stickit',
    'backbone.query.parameters' : '/assets/plugins/backbone.query.parameters'

   },

  'shim' : {
    'backbone' : {
      'deps' : ['underscore', 'jquery'],
      'exports' : 'Backbone'
    },
    'backbone.stickit' : ['backbone'],
    'underscore' : {
      'exports' : '_'
    },
    'marionette' : ['backbone']
    }

});
